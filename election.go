package main

import (
	"fmt"
	"strings"
	"sync"

	"github.com/cbergoon/merkletree"
	"github.com/libp2p/go-libp2p"
	"github.com/libp2p/go-libp2p-core/peer"
	"github.com/mr-tron/base58/base58"
)

type Election struct {
	sync.RWMutex

	Candidates      []Candidate
	// for slave: signifies when election was closed by master
	//
	// for master: signifies when user hits ENTER to close the election
	//
	// the number of peers known by master is passed through it
	close           chan<- int
	// used by handleCmd to prevent closing election more than once
	closed          bool
	electionKey     string
	localVoter      *LocalVoter
	masterID        peer.ID
	merkleRoot      []byte
	remoteVoters    map[peer.ID]*Voter
	rendezvousNonce Nonce
}

func NewElectionWithCandidates(candidates []Candidate) *Election {
	localVoter := NewLocalVoter(libp2p.RandomIdentity)
	e := &Election{
		Candidates:   candidates,
		localVoter:   localVoter,
		masterID:     localVoter.ID(),
		remoteVoters: make(map[peer.ID]*Voter),
	}
	e.rendezvousNonce = NewNonce()
	content := []merkletree.Content{e.rendezvousNonce}
	for _, cand := range e.Candidates {
		content = append(content, cand)
	}
	optionsMerkle, err := merkletree.NewTree(content)
	if err != nil {
		panic(err)
	}
	e.merkleRoot = optionsMerkle.MerkleRoot()
	e.electionKey = fmt.Sprintf("%s0%s",
		base58.Encode(e.merkleRoot),
		localVoter.ID())
	return e
}

func NewElectionWithElectionKey(electionKey string) *Election {
	var (
		candidates      []Candidate
		localVoter      *LocalVoter
		rendezvousNonce Nonce
	)
	localVoter = NewLocalVoter(libp2p.RandomIdentity)
	e := &Election{
		Candidates:      candidates,
		electionKey:     electionKey,
		localVoter:      localVoter,
		remoteVoters:    make(map[peer.ID]*Voter),
		rendezvousNonce: rendezvousNonce,
	}
	zeroi := strings.IndexByte(electionKey, '0')
	if zeroi == -1 {
		panic("invalid election key")
	}
	logger.Info("merkle root:", electionKey[:zeroi])
	var err error
	e.merkleRoot, err = base58.Decode(electionKey[:zeroi])
	if err != nil {
		panic(err)
	}
	e.masterID, err = peer.Decode(electionKey[zeroi+1:])
	if err != nil {
		panic(err)
	}
	logger.Info("master ID:", e.masterID)
	return e
}
