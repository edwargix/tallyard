package main

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"math/big"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/libp2p/go-libp2p-core/host"
	"github.com/libp2p/go-libp2p-core/network"
	"github.com/libp2p/go-libp2p-core/peer"
	discovery "github.com/libp2p/go-libp2p-discovery"
	dht "github.com/libp2p/go-libp2p-kad-dht"
	"github.com/mr-tron/base58/base58"
	"github.com/multiformats/go-multiaddr"
)

type Voter struct {
	sum      *big.Int
	// may get more than 1 eval from peer; doesn't need to be RW because we
	// never serve it to peers
	inputMu  sync.Mutex
	input    *big.Int
	output   *big.Int
	addrInfo peer.AddrInfo
}

type LocalVoter struct {
	Voter
	host.Host
	ctx    context.Context
	ballot []byte
	kdht   *dht.IpfsDHT
	poly   *Poly

	// mutexs only used for atomicity; atomicity.Value sucks because we lose
	// type safety with interface{}
	polyMu  sync.RWMutex // poly is computed after ballot; don't want R/W data races
	sumMu   sync.RWMutex // sum is computed in a loop; don
	inputMu sync.RWMutex // TODO remove by generating input right away
}

func stripNewline(str string) string {
	if str == "" {
		return str
	}
	if str[len(str)-1] == '\n' {
		return str[:len(str)-1]
	}
	return str
}

func handleCmd(cmd string, rw *bufio.ReadWriter, stream network.Stream, election *Election) {
	localVoter := election.localVoter
	switch cmd {
	case "info":
		rw.WriteString(fmt.Sprintf("%s\n", election.rendezvousNonce))
		for _, option := range election.Candidates {
			rw.WriteString(fmt.Sprintf("%s\n", option))
		}
		rw.Flush()
	case "close":
		election.Lock()
		defer election.Unlock()
		if peer := stream.Conn().RemotePeer(); peer != election.masterID {
			logger.Warning("received close command from non-master:", peer)
			return
		}
		if election.closed {
			logger.Warning("election already closed")
			return
		}
		str, err := rw.ReadString('\n')
		if err != nil && err != io.EOF {
			panic(err)
		}
		str = stripNewline(str)
		numPeers, err := strconv.Atoi(str)
		if err != nil {
			panic(err)
		}
		election.close <- numPeers
		election.closed = true
	case "shake":
		election.Lock()
		defer election.Unlock()
		peerID := stream.Conn().RemotePeer()
		if election.closed {
			logger.Warning("peer attempted to shake after "+
				"election was closed:", peerID)
			return
		}
		if _, exists := election.remoteVoters[peerID]; exists {
			logger.Warning("peer attempted to shake after having already done so", peerID)
			return
		}
		fmt.Printf("found voter: %s\n", peerID)
		election.remoteVoters[peerID] = &Voter{
			addrInfo: peer.AddrInfo{
				ID:    peerID,
				Addrs: []multiaddr.Multiaddr{stream.Conn().RemoteMultiaddr()},
			},
		}
	case "eval": // peer is giving their input and requesting output from our poly
		localVoter.polyMu.RLock()
		defer localVoter.polyMu.RUnlock()
		if localVoter.poly == nil {
			logger.Warning("peer attempted to eval before we had our poly:",
				stream.Conn().RemotePeer())
			return
		}
		inputb58, err := rw.ReadString('\n')
		if err != nil && err != io.EOF {
			logger.Warning("unable to read input from peer during eval:",
				stream.Conn().RemotePeer())
			return
		}
		inputb58 = stripNewline(inputb58)
		inputBytes, err := base58.Decode(inputb58)
		if err != nil {
			logger.Warning("unable to base58 decode input from peer during eval:",
				stream.Conn().RemotePeer())
			return
		}
		peer, exists := election.remoteVoters[stream.Conn().RemotePeer()]
		if !exists {
			logger.Warning("received eval command from unrecognized peer")
			return
		}
		peer.inputMu.Lock()
		defer peer.inputMu.Unlock()
		peer.input = new(big.Int).SetBytes(inputBytes)
		logger.Infof("%s input: %s", peer.addrInfo.ID, peer.input)
		output := localVoter.poly.Eval(peer.input)
		rw.WriteString(base58.Encode(output.Bytes()))
		rw.Flush()
	case "sum":
		localVoter.sumMu.RLock()
		defer localVoter.sumMu.RUnlock()
		if localVoter.sum == nil {
			logger.Info("peer attempted to fetch sum "+
				"before we computed it:", stream.Conn().RemotePeer())
			return
		}
		rw.WriteString(base58.Encode(localVoter.sum.Bytes()))
		rw.Flush()
	default:
		logger.Warningf("uknown command %s", cmd)
	}
}

func streamHandler(stream network.Stream, election *Election) {
	logger.Info("got a new stream:", stream)
	logger.Info("remote peer:", stream.Conn().RemotePeer())
	rw := bufio.NewReadWriter(bufio.NewReader(stream), bufio.NewWriter(stream))

	cmd, err := rw.ReadString('\n')
	if err != nil && err != io.EOF {
		panic(err)
	}
	cmd = stripNewline(cmd)

	logger.Info("cmd:", cmd)
	handleCmd(cmd, rw, stream, election)
	stream.Close()
}

func findPeers(closeElection <-chan int, election *Election) {
	localVoter := election.localVoter
	routingDiscovery := discovery.NewRoutingDiscovery(localVoter.kdht)
	logger.Info("announcing ourselves")
	discovery.Advertise(localVoter.ctx, routingDiscovery, string(election.rendezvousNonce))
	logger.Info("successfully announced!")

	fmt.Println("finding other voters...")
	peerChan, err := routingDiscovery.FindPeers(localVoter.ctx, string(election.rendezvousNonce))
	if err != nil {
		panic(err)
	}
	numPeers := -1
	for {
		if numPeers != -1 && numPeers == len(election.remoteVoters) {
			break
		}
		select {
		case peer := <-peerChan:
			if peer.ID == localVoter.ID() {
				continue
			}
			fmt.Printf("found voter: %s\n", peer.ID)
			logger.Info("connecting to:", peer)
			err = localVoter.Connect(localVoter.ctx, peer)
			if err != nil {
				logger.Warn("couldn't connect to peer: ", err)
				continue
			}
			stream, err := localVoter.NewStream(localVoter.ctx, peer.ID, protocolID)
			if err != nil {
				logger.Warn("couldn't open stream with peer: ", err)
				continue
			}
			writer := bufio.NewWriter(stream)
			writer.WriteString("shake")
			writer.Flush()
			stream.Close()
			election.remoteVoters[peer.ID] = &Voter{addrInfo: peer}
		case numPeers = <-closeElection:
			if len(election.remoteVoters) > numPeers {
				logger.Fatalf("found more peers than master! %d > %d",
					len(election.remoteVoters), numPeers)
				os.Exit(1)
			}
		}
	}
	logger.Info("done finding peers")
}

func (voter *Voter) fetchNumber(election *Election, cmd string, args ...string) *big.Int {
	printErr := func(err error, msg string) {
		logger.Errorf("%s: %s while fetching `%s'; retrying in 2 seconds",
			voter.addrInfo.ID, msg, cmd)
		time.Sleep(time.Second * 2)
	}
retry:
	localVoter := election.localVoter
	stream, err := localVoter.NewStream(localVoter.ctx, voter.addrInfo.ID, protocolID)
	if err != nil {
		printErr(err, "couldn't open stream")
		goto retry
	}
	rw := bufio.NewReadWriter(bufio.NewReader(stream), bufio.NewWriter(stream))
	out := cmd
	for _, arg := range args {
		out += "\n" + arg
	}
	_, err = rw.WriteString(out)
	if err != nil {
		printErr(err, "couldn't write to stream")
		goto retry
	}
	err = rw.Flush()
	if err != nil {
		printErr(err, "couldn't flush stream")
		goto retry
	}
	err = stream.Close() // only closes writing
	if err != nil {
		printErr(err, "couldn't close stream")
		goto retry
	}
	retB58, err := rw.ReadString('\n')
	if err != nil && err != io.EOF {
		printErr(err, "couldn't read string from stream")
		goto retry
	}
	retB58 = stripNewline(retB58)
	if retB58 == "" {
		printErr(err, "empty string")
		goto retry
	}
	retBytes, err := base58.Decode(retB58)
	if err != nil {
		printErr(err, "couldn't base58-decode contents from stream")
		goto retry
	}
	return new(big.Int).SetBytes(retBytes)
}

func (election *Election) StartVoting() {
	localVoter := election.localVoter

	var err error
	localVoter.inputMu.Lock()
	localVoter.input, err = RandomBigInt(128, false)
	localVoter.inputMu.Unlock()
	if err != nil {
		panic(err)
	}
	logger.Infof("our input: %s", localVoter.input)

	localVoter.ballot = vote(election.Candidates)
	logger.Infof("our ballot: %v", localVoter.ballot)

	// no +1 since we want degree k-1 where k is total number of voters
	localVoter.polyMu.Lock()
	localVoter.poly = NewRandomPoly(uint(len(election.remoteVoters)),
		1024, localVoter.ballot)
	localVoter.polyMu.Unlock()
	logger.Infof("our constant: %s", localVoter.poly.constant)

	// get outputs
	var wg sync.WaitGroup
	for _, voter := range election.remoteVoters {
		wg.Add(1)
		go func(voter *Voter) {
			voter.output = voter.fetchNumber(election, "eval", base58.Encode(localVoter.input.Bytes()))
			logger.Infof("voter %s output: %s", voter.addrInfo.ID, voter.output)
			wg.Done()
		}(voter)
	}
	wg.Wait()

	// calculate sum
	localVoter.sumMu.Lock()
	localVoter.sum = localVoter.poly.Eval(localVoter.input)
	for _, voter := range election.remoteVoters {
		localVoter.sum.Add(localVoter.sum, voter.output)
	}
	localVoter.sumMu.Unlock()
	logger.Infof("our sum: %s", localVoter.sum)

	// get sums
	for _, voter := range election.remoteVoters {
		wg.Add(1)
		go func(voter *Voter) {
			voter.sum = voter.fetchNumber(election, "sum")
			logger.Infof("voter %s sum: %s",
				voter.addrInfo.ID, voter.sum)
			wg.Done()
		}(voter)
	}
	wg.Wait()

	mat := constructPolyMatrix(election)
	mat.RREF()

	constant := mat[0][len(mat[0])-1]
	if !constant.IsInt() {
		panic("constant term is not an integer")
	}

	result := constant.Num().Bytes()

	// number of bytes we need to insert at the front since they're zero
	diff := (len(election.Candidates)*len(election.Candidates)) - len(result)
	result = append(make([]byte, diff), result...)

	printResults(result, election.Candidates)

	// temporary
	select {}
}

func constructPolyMatrix(election *Election) Matrix {
	mat := make(Matrix, len(election.remoteVoters) + 1) // row for everyone (including ourselves)

	i := 0
	for _, voter := range election.remoteVoters {
		mat[i] = make([]big.Rat, len(mat) + 1) // includes column for sum
		row := mat[i]
		row[0].SetInt64(1)
		var j int64
		for j = 1; j <= int64(len(election.remoteVoters)); j++ {
			row[j].SetInt(new(big.Int).Exp(voter.input, big.NewInt(j), nil))
		}
		row[j].SetInt(voter.sum)
		i++
	}

	// row for ourselves
	mat[i] = make([]big.Rat, len(mat) + 1)
	row := mat[i]
	row[0].SetInt64(1)
	localVoter := election.localVoter
	var j int64
	for j = 1; j <= int64(len(election.remoteVoters)); j++ {
		row[j].SetInt(new(big.Int).Exp(localVoter.input, big.NewInt(j), nil))
	}
	row[j].SetInt(localVoter.sum)

	return mat
}

func printResults(result []byte, candidates []Candidate) {
	logger.Infof("result: %v", result)
	fmt.Println("=== Results ===")
	n := len(candidates)
	for i, cand := range candidates {
		for j, vs := range candidates {
			if i != j {
				fmt.Printf("%s over %s: %d\n", cand, vs, result[i * n + j])
			}
		}
	}
}
