package main

import (
	"fmt"
	"os"
	"sort"
	"strconv"
	"unicode"

	"github.com/rivo/tview"
)

func tui() (election *Election) {
	app := tview.NewApplication()
	done := func(buttonIndex int, buttonLabel string) {
		app.Stop()
		switch buttonLabel {
		case "Create Election":
			candidates := getCandidatesTUI()
			// TODO: slaves should check that len candidates >= 2
			if len(candidates) == 0 {
				fmt.Printf("no candidates entered; exiting\n")
				os.Exit(0)
			}
			election = NewElectionWithCandidates(candidates)
		case "Join Election":
			electionKey := getElectionKeyTUI()
			if electionKey == "" {
				fmt.Printf("no election key given; exiting\n")
				os.Exit(0)
			}
			election = NewElectionWithElectionKey(electionKey)
		}
	}
	modal := tview.NewModal().
		SetText("Welcome to tallyard!").
		AddButtons([]string{"Create Election", "Join Election"}).
		SetDoneFunc(done)
	if err := app.SetRoot(modal, false).EnableMouse(true).Run(); err != nil {
		panic(err)
	}
	return election
}

func getCandidatesTUI() (candidates []Candidate) {
	var form *tview.Form
	n := 3
	app := tview.NewApplication()
	plus := func() {
		form.AddInputField(fmt.Sprintf("%d.", n), "", 50, nil, nil)
		n++
	}
	minus := func() {
		// TODO: ensure from joiner that there are at least two
		// candidates
		if n > 3 {
			form.RemoveFormItem(n - 2)
			n--
		}
	}
	done := func() {
		// TODO: ensure none of the candidates are empty
		app.Stop()
		for i := 0; i < n-1; i++ {
			candidates = append(candidates,
				Candidate(form.GetFormItem(i).(*tview.InputField).GetText()))
		}
	}
	form = tview.NewForm().
		AddInputField("1.", "", 50, nil, nil).
		AddInputField("2.", "", 50, nil, nil).
		AddButton("+", plus).
		AddButton("-", minus).
		AddButton("Done", done)
	if err := app.SetRoot(form, true).EnableMouse(true).Run(); err != nil {
		panic(err)
	}
	return candidates
}

func getElectionKeyTUI() (electionKey string) {
	app := tview.NewApplication()
	var form *tview.Form
	done := func() {
		app.Stop()
		electionKey = form.GetFormItem(0).(*tview.InputField).GetText()
	}
	form = tview.NewForm().
		AddInputField("Election key:", "", 100, nil, nil).
		AddButton("Continue", done)
	if err := app.SetRoot(form, true).EnableMouse(true).Run(); err != nil {
		panic(err)
	}
	return electionKey
}

// displays a voting UI to the user and returns the encoded ballot
func vote(candidates []Candidate) []byte {
	ranks := make([]int, len(candidates))
	app := tview.NewApplication()
	form := tview.NewForm()

	for _, eo := range candidates {
		// TODO: support more than 99 candidates
		form.AddInputField(string(eo), "", 2,
			func(textToCheck string, lastChar rune) bool {
				return len(textToCheck) < 3 && unicode.IsDigit(lastChar)
			}, nil)
	}

	form.AddButton("Submit", func() {
		app.Stop()
		for i := 0; i < len(candidates); i++ {
			rank, err := strconv.Atoi(form.GetFormItem(i).(*tview.InputField).GetText())
			if err != nil {
				panic(err)
			}
			ranks[i] = rank
		}
	})

	if err := app.SetRoot(form, true).EnableMouse(true).Run(); err != nil {
		panic(err)
	}

	return GetBallotFromRankings(ranks)
}

func GetBallotFromRankings(ranks []int) []byte {
	n := len(ranks)
	candidates := make([]int, n)

	for i := 0; i < n; i++ {
		candidates[i] = i
	}

	// sort candidates by ranking
	cr := CandidateRanking{candidates, ranks}
	sort.Sort(&cr)

	// TODO: support more than 255 voters (limit from usage of byte)
	prefMatrix := make([][]byte, n)

	for len(candidates) > 0 {
		r := ranks[candidates[0]]
		i := 1
		for i < len(candidates) && ranks[candidates[i]] == r {
			i++
		}
		// i is now index of the first candidate with worse (i.e. higher
		// in value) rank
		row := make([]byte, n)
		for j := i; j < len(candidates); j++ {
			row[candidates[j]] = 1
		}
		for j := 0; j < i; j++ {
			prefMatrix[candidates[j]] = row
		}
		candidates = candidates[i:]
	}

	// convert 2D array into 1D array
	barray := make([]byte, 0, n*n)
	for _, row := range prefMatrix {
		barray = append(barray, row...)
	}

	return barray
}

type CandidateRanking struct {
	candidates []int // becomes list of candidate IDs sorted by rank
	ranks      []int // maps candidate ID to rank
}

func (cr *CandidateRanking) Len() int {
	return len(cr.ranks)
}

func (cr *CandidateRanking) Less(i, j int) bool {
	return cr.ranks[cr.candidates[i]] < cr.ranks[cr.candidates[j]]
}

func (cr *CandidateRanking) Swap(i, j int) {
	tmp := cr.candidates[i]
	cr.candidates[i] = cr.candidates[j]
	cr.candidates[j] = tmp
}
