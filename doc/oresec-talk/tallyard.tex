\documentclass{beamer}
\usepackage{nth}
\usepackage{minted}
\usepackage{csquotes}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{amssymb}


\title{Securing Elections using Homomorphic Cryptography}
\author{David Florness}
\date{February \nth{10}, 2020}


\usetheme{Dresden}
% \usecolortheme{seahorse}


\begin{document}
\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Voting}
  \begin{itemize}
  \item still most done via paper ballots \pause
  \item why is that?
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]{The Traditional Approach in Electronic Terms}

  \begin{enumerate}
  \item voters register with a server
  \item once authenticated, voters send their ballots to the server
  \item the server counts the votes
  \item the aggregate result is published
  \end{enumerate}

  \framebreak

  \begin{center}
    \includegraphics[height=0.8\textheight]{graphics/traditional-no-arrows.png}
  \end{center}

  \framebreak

  \begin{center}
    \includegraphics[height=0.8\textheight]{graphics/traditional.png}
  \end{center}
\end{frame}

\begin{frame}
  \begin{itemize}
  \item Pros \pause
    \begin{enumerate}
    \item easy and simple
    \item reliable
    \item allows registration
    \end{enumerate} \pause
  \item Cons
    \begin{enumerate}
    \item trust in a single authority; such a system would be rife with
      corruption
    \end{enumerate}
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{itemize}
  \item We want an election system that \pause
    \begin{itemize}
    \item requires registration / authentication, allowing us to limit who can
      vote \pause
      \begin{itemize}
      \item we may want to limit voting to members of a club, citizens of a
        jurisdiction, etc. \pause
      \item without such authentication, stopping double-voting would be a
        nightmare
      \end{itemize} \pause
    \item gives accurate aggregate results \pause
    \item protects voter privacy \pause
      \begin{itemize}
      \item no individual voter's vote should be knowable by anyone except said
        voter \pause
      \end{itemize}
    \end{itemize}
  \end{itemize}
  \dots these sound like conflicting goals\dots \pause but they're not!
\end{frame}

\begin{frame}
  \begin{center}
    \textbf{Disclaimer}: I did not come up with any of this.
  \end{center}
\end{frame}

\begin{frame}[allowframebreaks]{A New Approach}
  \begin{itemize}
  \item instead of a client-server approach, let's have a peer-to-peer (p2p)
    network where voters connect to each other directly
  \end{itemize}

  \framebreak

  \begin{center}
    \includegraphics[height=0.8\textheight]{graphics/p2p.png}
  \end{center}
\end{frame}

\begin{frame}
  \begin{itemize}
  \item Authentication is essentially the same as before: voters use standard
    means to identify themselves (think passwords, kerberos, LDAP, SSH/GPG
    finderprints, multipass, etc.)
  \end{itemize}
\end{frame}

\begin{frame}{The big question}
  \begin{itemize}
  \item \textbf{Question}: how and to whom do we submit ballots? \pause
  \item \textbf{Answer}: we ``share'' pieces of our secret ballot with everyone!
  \end{itemize}
\end{frame}

\begin{frame}{Introducing secret sharing}
  \begin{itemize}
  \item
    \begin{quote}
      \textbf{Secret sharing} (also called secret splitting) refers to methods
      for distributing a secret amongst a group of participants, each of whom is
      allocated a share of the secret. The secret can be reconstructed only when
      a sufficient number, of possibly different types, of shares are combined
      together; individual shares are of no use on their
      own.~\footnote{\url{https://en.wikipedia.org/wiki/Secret\_sharing}}
    \end{quote}
  \end{itemize}
\end{frame}

\begin{frame}{Preliminaries}
  \begin{itemize}
  \item First, let's have every voter generate and share a random natural number
    that will serve as said voter's public key. Let $x_i$ denote this random
    number for voter $i$. \pause
    \begin{center}
      \includegraphics[height=0.6\textheight]{graphics/dist-x_i.png}
    \end{center}
  \end{itemize}
\end{frame}

\begin{frame}{Polynomials}
  \begin{itemize}
  \item We're now going to leverage plain old polynomials to conduct secret
    sharing. \pause
  \item Let's have every voter, say voter $i$, create a secret polynomial $P_i$
    of degree $k-1$ where $k$ is the total number of voters:
    \begin{equation*}
      P_i(x) = c_i + a_{(i,1)} x + a_{(i,2)} x^2 + \dots + a_{(i,k-1)} x^{k-1}
    \end{equation*}
  \item \pause Every $a_{(i,j)}$ is a random natural number. \pause
  \item $c_i$ is the numerically-encoded contents of voter $i$'s ballot. Every
    $c_i$ must be encoded in such a way that summing every $\{c_j\}_{j\in[1,k]}$
    results in the desired election result. For example, in a simple ``yes/no''
    election, 1 could represent ``yes'' and -1 could represent ``no''.
  \end{itemize}
\end{frame}

\begin{frame}{Polynomials as sharable secrets}
  \begin{itemize}
  \item \pause To know everything about a line (degree 1 polynomial), we only
    need two points on the it. \pause
  \item To know everything about a parabola curve (degree 2 polynomial), we only
    need three points on the curve. \pause
  \item $\dots$ \pause
  \item As you'd expect, this pattern continues: to know everything about a
    $k$-degree polynomial, we need only $k+1$ points. \pause
  \item Thus, knowing everything about our $k-1$ degree polynomial $P_i$ is
    equivalent to knowing $k$ points on it. \pause
  \item So, we're going to distribute our secret by giving everyone just one
    point.
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{itemize}
  \item Every voter sends every other voter the result of evulating their
    polynomial at the given voter's public input:
    \begin{center}
      \includegraphics[height=0.8\textheight]{graphics/dist-P_i.png}
    \end{center}
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{center}
    \includegraphics[height=0.8\textheight]{graphics/dist-P(x_i).png}
  \end{center}
\end{frame}

\begin{frame}[allowframebreaks]
  \begin{itemize}
  \item Voter $i$ now has:
    \begin{itemize}
    \item $P_1(x_i)$
    \item $P_2(x_i)$
    \item $\vdots$
    \item $P_k(x_i)$
    \end{itemize}
  \end{itemize}

  \framebreak

  Let's sum those values!
  \begin{align*}
    S_i = &P_1(x_i) + P_2(x_i) + \cdots + P_k(x_i) \\
    = &\left( c_1 + a_{(1,1)} x_i + a_{(1,2)} x_i^2 + \cdots + a_{(1,k-1)} x_i^{k-1} \right) + \\
          &\left( c_2 + a_{(2,1)} x_i + a_{(2,2)} x_i^2 + \cdots + a_{(2,k-1)} x_i^{k-1} \right) + \\
          &\cdots \\
          &\left( c_k + a_{(k,1)} x_i + a_{(k,2)} x_i^2 + \cdots + a_{(k,k-1)} x_i^{k-1} \right) \\
    = &\sum_{j=1}^k c_j + x_i \sum_{j=1}^k a_{(j,1)} + x_i^2 \sum_{j=1}^k a_{(j,2)} + \cdots + x_i^{k-1} \sum_{j=1}^k a_{(j,2)}
  \end{align*}
\end{frame}

\begin{frame}
  Let's have every voter share their sum with everyone else.
  \begin{center}
    \includegraphics[height=0.8\textheight]{graphics/dist-S_i.png}
  \end{center}
\end{frame}

\begin{frame}
  \begin{itemize}
  \item Everyone now has $S_1$, $S_2$, $\dots$, $S_k$: \pause
    \begin{align*}
      S_1 &= \sum_{j=1}^k c_j + x_1 \sum_{j=1}^k a_{(j,1)} + x_1^2 \sum_{j=1}^k a_{(j,2)} + \cdots + x_1^{k-1} \sum_{j=1}^k a_{(j,k-1)} \\
      S_1 &= \sum_{j=1}^k c_j + x_2 \sum_{j=1}^k a_{(j,1)} + x_2^2 \sum_{j=1}^k a_{(j,2)} + \cdots + x_2^{k-1} \sum_{j=1}^k a_{(j,k-1)} \\
          &\vdots \\
      S_k &= \sum_{j=1}^k c_j + x_k \sum_{j=1}^k a_{(j,1)} + x_k^2 \sum_{j=1}^k a_{(j,2)} + \cdots + x_k^{k-1} \sum_{j=1}^k a_{(j,k-1)}
    \end{align*} \pause
  \item These are $k$ points all on the same $k-1$ degree polynomial:
    \begin{equation*}
      (x_1, S_1), (x_2, S_2), \dots, (x_k, S_k)
    \end{equation*}
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{itemize}
  \item Since we know $k$ points of the $k-1$ degree polynomial, we can find the
    coefficients and constant term of it with a little linear algebra: \pause
    \begin{equation*}
      \begin{bmatrix}
        1 & x_1 & x_1^2 & \cdots & x_1^{k-1} \\
        1 & x_2 & x_2^2 & \cdots & x_2^{k-1} \\
        \vdots & & & \ddots \\
        1 & x_k & x_k^2 & \cdots & x_k^{k-1}
      \end{bmatrix}
            \begin{bmatrix}
              \sum_{j=1}^k c_j \\
              \sum_{j=1}^k a_{(j,1)} \\
              \sum_{j=1}^k a_{(j,2)} \\
              \vdots \\
              \sum_{j=1}^k a_{(j,k-1)}
            \end{bmatrix} =
            \begin{bmatrix}
              S_1 \\
              S_2 \\
              \vdots \\
              S_k
            \end{bmatrix}
    \end{equation*}
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{equation*}
    \text{RREF}\left(
      \begin{bmatrix}
        1 & x_1 & x_1^2 & \cdots & x_1^{k-1} & S_1 \\
        1 & x_2 & x_2^2 & \cdots & x_2^{k-1} & S_2 \\
        \vdots & & & \ddots \\
        1 & x_k & x_k^2 & \cdots & x_k^{k-1} & S_k
      \end{bmatrix}
    \right)
  \end{equation*}
\end{frame}

\begin{frame}
  Notice that the constant term of the final polynomial is $\sum_{j=1}^k c_j$,
  which is precisely the result of the election!
\end{frame}

\begin{frame}{In Summary}
  \begin{enumerate}
  \item Every voter creates and publishes an $x_i$. \pause
  \item Every voter creates a random polynomial of degree $k-1$ where $k$ is the
    number of voters and embeds their ballot in the constant term of the
    polynomial. \pause
  \item Every voter evaluates their polynomial with each of the inputs ($x_i$'s)
    and sends each respective voter his/her result. \pause
  \item Every voter sums the polynomial outputs they've received. \pause
  \item All $k$ sums the voters compute are used to find the constant term of a
    polynomial whose constant term is precisely the sum of all ballot.
  \end{enumerate}
\end{frame}

\begin{frame}
  \begin{center}
    \huge{Demo}
  \end{center}
\end{frame}

\begin{frame}
  \begin{center}
    There's a problem\dots \\[0.5cm] \pause

    What's to stop someone from putting an invalid ballot in the constant term
    of their polynomial that sways the election in their favor? \\[0.5cm] \pause

    For example, in a ``yes/no'' election, someone could put 2 for their ballot
    and have the result of 2 votes.
  \end{center}
\end{frame}

\begin{frame}{The Remedy}
  Where I got all of the following material: \pause

  \url{https://vitalik.ca/general/2017/11/09/starks\_part\_1.html} \pause

  The following material was very rushed.
\end{frame}

\begin{frame}
  \begin{itemize}
  \item We want to prove that a given $P_i$ constant term is valid
    \textit{without} revealing what it is. This is equivalent to checking
    whether $P_i(0)$ is valid. This is the essence of a Zero-Knowledge Proof.
    \pause
  \item Now, let $C(x)$ be a \textit{constraint checking polynomial} that is
    zero if $x$ is a valid constant and nonzero otherwise. For example, if we
    assume a valid constant is one that is either a zero or one, we can
    construct $C(x)$ very simply:
    \begin{equation*}
      C(x) = (x - 0) (x - 1) = x^2 - x
    \end{equation*} \pause
  \item Now, we can restate the problem as: we need to prove that $C(P(x)) = 0$
    when $x = 0$.
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{itemize}
  \item Let $Z(x) = x$ \pause
  \item It's a known mathematical fact that any polynomial that is zero at $x=0$
    must be a multiple of $Z$. Therefore, there exists some $D(x)$ such that
    \begin{equation*}
      C(P(x)) =  Z(x) \cdot D(x)
    \end{equation*}
  \item \pause Before anything, everyone ``commits''~\footnote{see commitment
      schemes} to their polynomial by creating a merkle tree of the outputs of
    $P(x)$ and $D(x)$ values and sending the root of the tree to
    everyone.
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{center}
    \includegraphics[height=0.8\textheight]{graphics/merkle.png}
  \end{center}
\end{frame}

\end{document}