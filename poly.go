package main

import (
	"crypto/rand"
	"math"
	"math/big"
)

type Poly struct {
	constant *big.Int
	coefs    []*big.Int
}

func RandomBigInt(numBytes uint, allowAllZeros bool) (*big.Int, error) {
	randBytes := make([]byte, numBytes)
	_, err := rand.Read(randBytes)
	if err != nil {
		return nil, err
	}
	bi := new(big.Int).SetBytes(randBytes)
	if !allowAllZeros && bi.Cmp(big.NewInt(0)) == 0 {
		return RandomBigInt(numBytes, allowAllZeros)
	}
	return bi, nil
}

func NewPoly(coefficients []*big.Int, ballot []byte) *Poly {
	constant := new(big.Int).SetBytes(ballot)
	return &Poly{constant, coefficients}
}

// creates a random polynomial of the given degree, uses at least `entropy' bits
// of entropy for the random coefficients, and assigns the ballot to the
// constant term
func NewRandomPoly(degree uint, entropy uint, ballot []byte) *Poly {
	coefficients := make([]*big.Int, degree)

	// number of bits per coefficient
	numBits := uint(math.Ceil(float64(entropy) / float64(degree)))

	// number of bytes per coefficient
	numBytes := numBits / 8
	if numBits%8 > 0 {
		numBytes += 1
	}

	var err error
	for i := range coefficients {
		coefficients[i], err = RandomBigInt(numBytes, false)
		if err != nil {
			panic(err)
		}
	}

	return NewPoly(coefficients, ballot)
}

func (p *Poly) Eval(input *big.Int) *big.Int {
	res := new(big.Int).Set(p.constant)

	for i, coef := range p.coefs {
		degree := big.NewInt(int64(i + 1))
		term := new(big.Int).Exp(input, degree, nil)
		term.Mul(term, coef)
		res.Add(res, term)
	}

	return res
}
