module tallyard.xyz

go 1.13

require (
	github.com/cbergoon/merkletree v0.2.0
	github.com/ipfs/go-log v1.0.2
	github.com/libp2p/go-libp2p v0.6.1
	github.com/libp2p/go-libp2p-core v0.5.0
	github.com/libp2p/go-libp2p-discovery v0.2.0
	github.com/libp2p/go-libp2p-kad-dht v0.5.0
	github.com/mr-tron/base58 v1.2.0
	github.com/multiformats/go-multiaddr v0.2.1
	github.com/rivo/tview v0.0.0-20200528200248-fe953220389f
)
