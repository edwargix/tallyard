package main

import (
	"bytes"
	"crypto/rand"
	"crypto/sha256"
	"fmt"
	"os"

	"github.com/cbergoon/merkletree"
	"github.com/mr-tron/base58/base58"
)

type Candidate string

func (eo Candidate) CalculateHash() ([]byte, error) {
	h := sha256.New()
	if _, err := h.Write([]byte(eo)); err != nil {
		return nil, err
	}
	return h.Sum(nil), nil
}

func (eo Candidate) Equals(other merkletree.Content) (bool, error) {
	return eo == other.(Candidate), nil
}

type Nonce string

func NewNonce() Nonce {
	randBytes := make([]byte, 128)
	_, err := rand.Read(randBytes)
	if err != nil {
		panic(err)
	}
	return Nonce(base58.Encode(randBytes))
}

func (n Nonce) CalculateHash() ([]byte, error) {
	h := sha256.New()
	b, err := base58.Decode(string(n))
	if err != nil {
		return nil, err
	}
	if _, err = h.Write(b); err != nil {
		return nil, err
	}
	return h.Sum(nil), nil
}

func (n Nonce) Equals(other merkletree.Content) (bool, error) {
	return n == other.(Nonce), nil
}

func verifyElectionInfo(election *Election, merkleRoot []byte) {
	content := []merkletree.Content{election.rendezvousNonce}
	var err error
	for _, eo := range election.Candidates {
		content = append(content, eo)
	}
	optionsMerkle, err := merkletree.NewTree(content)
	if err != nil {
		panic(err)
	}
	if bytes.Compare(optionsMerkle.MerkleRoot(), merkleRoot) == 0 {
		fmt.Println("election info verification succeeded!")
	} else {
		fmt.Println("election info verification failed; exiting")
		os.Exit(1)
	}
}
