package main

import (
	"math/big"
	"testing"
)

func TestRREF(t *testing.T) {
	mat := Matrix{
		{*big.NewRat( 1, 1), *big.NewRat(2, 1), *big.NewRat(-1, 1), *big.NewRat( -4, 1)},
		{*big.NewRat( 2, 1), *big.NewRat(3, 1), *big.NewRat(-1, 1), *big.NewRat(-11, 1)},
		{*big.NewRat(-2, 1), *big.NewRat(0, 1), *big.NewRat(-3, 1), *big.NewRat( 22, 1)},
	}
	mat.RREF()
	result := Matrix{
		{*big.NewRat(1, 1), *big.NewRat(0, 1), *big.NewRat(0, 1), *big.NewRat(-8, 1)},
		{*big.NewRat(0, 1), *big.NewRat(1, 1), *big.NewRat(0, 1), *big.NewRat( 1, 1)},
		{*big.NewRat(0, 1), *big.NewRat(0, 1), *big.NewRat(1, 1), *big.NewRat(-2, 1)},
	}
	if !mat.Equals(result) {
		t.Fail()
	}

	mat = Matrix{
		{*big.NewRat(1, 1), *big.NewRat(3, 1), *big.NewRat(-1, 1)},
		{*big.NewRat(0, 1), *big.NewRat(1, 1),  *big.NewRat(7, 1)},
	}
	mat.RREF()
	result = Matrix{
		{*big.NewRat(1, 1), *big.NewRat(0, 1), *big.NewRat(-22, 1)},
		{*big.NewRat(0, 1), *big.NewRat(1, 1), *big.NewRat(  7, 1)},
	}
	if !mat.Equals(result) {
		t.Fail()
	}
}
