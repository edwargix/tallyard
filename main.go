package main

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"io"
	"os"
	"sync"

	"github.com/ipfs/go-log"
	"github.com/libp2p/go-libp2p"
	"github.com/libp2p/go-libp2p-core/host"
	"github.com/libp2p/go-libp2p-core/network"
	"github.com/libp2p/go-libp2p-core/peer"
	"github.com/libp2p/go-libp2p-core/protocol"
	"github.com/libp2p/go-libp2p-core/routing"
	dht "github.com/libp2p/go-libp2p-kad-dht"
)

const protocolID = protocol.ID("/tallyard/0.0.0")

var logger = log.Logger("tallyard")

func NewLocalVoter(hostOpts ...libp2p.Option) *LocalVoter {
	localVoter := new(LocalVoter)
	localVoter.ctx = context.Background()

	routing := libp2p.Routing(func(h host.Host) (routing.PeerRouting, error) {
		var err error
		localVoter.kdht, err = dht.New(localVoter.ctx, h)
		if err != nil {
			return localVoter.kdht, err
		}
		logger.Info("boostrapping the DHT")
		if err = localVoter.kdht.Bootstrap(localVoter.ctx); err != nil {
			panic(err)
		}
		return localVoter.kdht, err
	})

	var err error
	localVoter.Host, err = libp2p.New(localVoter.ctx, append(hostOpts, routing)...)
	if err != nil {
		panic(err)
	}

	logger.Info("host: ", localVoter.ID())
	logger.Info("addrs: ", localVoter.Addrs())

	return localVoter
}

func (localVoter *LocalVoter) Bootstrap() {
	var wg sync.WaitGroup
	for _, peerAddr := range dht.DefaultBootstrapPeers {
		peerInfo, _ := peer.AddrInfoFromP2pAddr(peerAddr)
		wg.Add(1)
		go func() {
			defer wg.Done()
			if err := localVoter.Connect(localVoter.ctx, *peerInfo); err != nil {
				logger.Warning(err)
			} else {
				logger.Info("connection established with bootstrap node:", *peerInfo)
			}
		}()
	}
	wg.Wait()
}

func (election *Election) SetupMaster() {
	fmt.Println("share this with peers:")
	fmt.Printf("%s\n", election.electionKey)

	logger.Info("waiting for incoming streams and finding voters...")

	election.Lock()
	ch := make(chan int, 1)
	election.close = ch
	go findPeers(ch, election)
	election.Unlock()

	localVoter := election.localVoter
	localVoter.SetStreamHandler(protocolID, func(stream network.Stream) {
		streamHandler(stream, election)
	})

	fmt.Println("press ENTER to solidify group of voters and start voting")
	_, err := bufio.NewReader(os.Stdin).ReadString('\n')
	if err != nil {
		panic(err)
	}

	logger.Info("ENTER has been pressed; closing election")
	election.Lock()
	n := len(election.remoteVoters)
	election.close <- n
	close(election.close)
	election.Unlock()
	election.RLock()
	for _, voter := range election.remoteVoters {
		stream, err := localVoter.NewStream(localVoter.ctx, voter.addrInfo.ID, protocolID)
		if err != nil {
			panic(err)
		}
		writer := bufio.NewWriter(stream)
		writer.WriteString(fmt.Sprintf("close\n%d", n))
		writer.Flush()
		stream.Close()
	}
	election.RUnlock()
}

func (election *Election) SetupSlave() {
	localVoter := election.localVoter

	// have candidates and nonce been loaded from elections file?
	// if not, fetch them from master
	if len(election.Candidates) == 0 || election.rendezvousNonce == "" {
		logger.Info("attempting to open stream with master peer...")
		stream, err := localVoter.NewStream(localVoter.ctx, election.masterID, protocolID)
		rw := bufio.NewReadWriter(bufio.NewReader(stream), bufio.NewWriter(stream))
		if err != nil {
			panic(err)
		}
		logger.Info("opened stream with master peer")

		logger.Info("fetching election info from master")
		_, err = rw.WriteString("info")
		if err != nil {
			panic(err)
		}
		rw.Flush()
		stream.Close() // only stops writing
		// first field is the rendezvous string, which is used for peer
		// discovery
		str, err := rw.ReadString('\n')
		if err != nil && err != io.EOF {
			panic(err)
		}
		str = stripNewline(str)
		election.rendezvousNonce = Nonce(str)
		// remaining fields are the candidates
		for {
			str, err := rw.ReadString('\n')
			if err != nil && err != io.EOF {
				panic(err)
			}
			str = stripNewline(str)
			if str != "" {
				election.Candidates = append(election.Candidates, Candidate(str))
			}
			if err == io.EOF {
				break
			}
		}
		logger.Info("done fetching election info")
	}

	logger.Info("checking authenticity of election info...")
	verifyElectionInfo(election, election.merkleRoot)

	// channel used to signify when election is closed
	ch := make(chan int, 1)
	election.close = ch
	// now that we have election info, begin handling streams
	localVoter.SetStreamHandler(protocolID, func(stream network.Stream) {
		streamHandler(stream, election)
	})

	findPeers(ch, election)
}

func main() {
	log.SetAllLoggers(log.LevelWarn)

	verbose := flag.Bool("v", false, "enable verbose logging for debugging")
	master := flag.Bool("m", false, "indicate that this node is the "+
		"master and the candidates are given via positional arguments")
	flag.Parse()
	if *verbose {
		log.SetLogLevel("tallyard", "info")
	} else {
		log.SetLogLevel("dht", "critical")
		log.SetLogLevel("relay", "critical")
		log.SetLogLevel("tallyard", "critical")
	}

	var election *Election

	if *master {
		// we are the master and the candidates are the positional
		// arguments
		candidates := []Candidate{}
		for _, candidate := range flag.Args() {
			candidates = append(
				candidates,
				Candidate(candidate))
		}
		// TODO: ensure at least 2 candidates
		election = NewElectionWithCandidates(candidates)
	} else if electionKey := flag.Arg(0); electionKey != "" {
		// we are a slave the and election key was given via CLI args
		election = NewElectionWithElectionKey(electionKey)
	} else {
		// create/join election via TUI
		election = tui()
		if election == nil {
			// tui form wasn't submitted (maybe the user hit ^C)
			os.Exit(0)
		}
	}

	election.localVoter.Bootstrap()

	if election.masterID == election.localVoter.ID() {
		election.SetupMaster()
	} else {
		election.SetupSlave()
	}

	election.StartVoting()
}
